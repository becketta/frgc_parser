import sys, os
from PIL import Image
from areader import AbstractParser

class FrgcToChamview( AbstractParser ):

	def write( self, outfile_name, dir_name=None, id_list=None, limit=None ):
		self.points_data = []
		point_kind_file = ''.join( outfile_name.split('.')[:-1] ) + '_pkinds.txt'
		self.point_kind_data = set()
	
		if id_list:
			for ID, session in self.registry.items():
				if ID in id_list:
					self.store( session, dir_name )
		elif limit:
			iterator = self.registry.iteritems()
			for i in range(limit):
				try:
					ID, session = iterator.next()
				except StopIteration:
					break
				self.store( session, dir_name )
		else:
			for ID, session in self.registry.items():
				self.store( session, dir_name )

		with open( outfile_name, 'w' ) as outfile:
			for line in sorted( self.points_data, key=lambda line: line.split(',')[0] ):
				outfile.write(line)
		with open( point_kind_file, 'w' ) as outfile:
			for kind in self.point_kind_data:
				outfile.write( kind + '\n' )

	def store( self, session, directory ):
		for point in session.points_record.points:
			name = point[0]
			self.point_kind_data.add( name )
		filename = session.image_filename
		if directory:
			if directory[-1] not in ['/','\\']:
				directory += os.path.sep
			filename = self.store_image( session.image_filename, directory )
		self.store_points( session, filename )

	def store_points( self, session, im_filename ):
		for name,x,y in session.points_record.points:
			line = str( im_filename ) + ','
			line += str( name ) + ','
			line += str( x ) + ',' + str( y ) + ',\n'
			self.points_data.append( line )

	def store_image( self, image_path, directory ):
		image = Image.open( image_path )
		name = image_path.split(os.path.sep)[-2:]
		name = '_'.join(name)
		image.save( directory + name )
		return directory + name


def main():
	converter = FrgcToChamview()
	converter.write( os.getcwd() + '/points.txt', dir_name="/mnt/home/becketta/pics/", limit=10 )

if __name__ == '__main__':
	sys.exit(main())


