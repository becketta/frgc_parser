from optparse import OptionParser

PATH_TO_ND1 = "/mnt/research/prip3did/"
BEE_DIST_PATH = "/mnt/research/prip3did/BEE_DIST/"
SIGNATURE_SET = "/mnt/research/prip3did/BEE_DIST/FRGC2.0/signature_sets/experiments/FRGC_Exp_2.0.4_Orig.xml"


# parse command line arguments
parser = OptionParser()
parser.add_option( '-n', '--nd1', dest='path_to_nd1', metavar='PATH',
		   help='full path to directory CONTAINING the nd1 folder' )
parser.add_option( '-b', '--bee-dist', dest='path_to_bee_dist', metavar='PATH',
		   help='full path of the BEE_DIST directory' )
parser.add_option( '-s', '--sig-set', dest='signature_set', metavar='PATH',
		   help='full path of the desired signature_set' )
parser.add_option( '-e', '--experiment', dest='experiment_num', metavar='NUM',
		   help='FRGC experiment number to use [ex: 2.0.4]' )
parser.add_option( '-t', '--experiment-type', dest='experiment_type', metavar='TYPE',
		   help='FRGC experiment type (Orig, Qeury, Target, or Training)' )

options, args = parser.parse_args()


# get parsed arguments
if options.path_to_nd1:
	PATH_TO_ND1 = options.path_to_nd1

if options.path_to_bee_dist:
	BEE_DIST_PATH = options.path_to_bee_dist

METADATA_PATH = BEE_DIST_PATH + "FRGC2.0/metadata/FRGC_2.0_Metadata.xml"

if options.signature_set:
	SIGNATURE_SET = options.signature_set

if options.signature_set and ( options.experiment_num or options.experiment_type ):
	parser.error("cannot specify the signature set AND experiment num/type")
elif options.experiment_num and options.experiment_type:
	SIGNATURE_SET = str( BEE_DIST_PATH + 
		    	 'FRGC2.0/signature_sets/experiments/FRGC_Exp_' + 
		    	 options.experiment_num + '_' +
		    	 options.experiment_type + '.xml' )
elif options.experiment_num or options.experiment_type:
	parser.error("options -e and -t are mutually inclusive")


