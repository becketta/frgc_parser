from objects import FrgcObject

class AbstractParser:

	def __init__(self):
		self.registry = self.read()

	def read(self):
		reader = FrgcObject()
		return reader.database

	def write(self):	
		'''Abstract class overriden by user designed frgc database
		converters. This class should save frgc data into the user's 
		desired format for easier use in the future.'''
		pass


