import xml.etree.ElementTree as ETree

from constants import PATH_TO_ND1, METADATA_PATH, SIGNATURE_SET


class FrgcObject:

	def __init__(self):
		# build file paths 
		metadata = METADATA_PATH
		signatures = SIGNATURE_SET
	
		point_container = AnchorPointsContainer( metadata )

		sig_file = open( signatures, 'r' )
		sig_tree = ETree.parse(sig_file)
		sig_file.close()
	
		self.database = {}
		for parent in self.iterparent(sig_tree):
			subject_id = parent.attrib['name']
			for child in parent:
				recording_id = child.attrib['name']
				image_file = PATH_TO_ND1 + child.attrib['file-name']
				image_format = child.attrib['file-format']
				self.database[ recording_id ] = FrgcSession( subject_id,
															 recording_id,
															 image_file,
															 image_format,
															 point_container )

	def iterparent(self, tree):
		for parent in tree.iter():
			if parent.tag.split('}')[-1] != 'biometric-signature': continue			
			yield parent


class FrgcSession:
    
	def __init__(self, s_id='', m_id='', im_file='', im_format='', points={}):
		self.subject_ID = s_id
		self.image_filename = im_file
		self.image_file_format = im_format
		try:
			self.points_record = points.data[m_id]
		except KeyError:
			self.points_record = None


class AnchorPointsContainer:
    
	def __init__(self, metadata_file):
		points_file = open( metadata_file, 'r' )
		point_tree = ETree.parse(points_file)
		points_file.close()

		self.data = {}
		for parent, child in self.iterparent(point_tree):
			recording_id = child.attrib['recording_id']
			#subject_id = child.attrib['subject_id']
			capturedate = child.attrib['capturedate']
			points = []
			for subchild in child:
				name = subchild.tag
				x = subchild.attrib['x']
				y = subchild.attrib['y']
				points.append(( name, x, y ))
			record = Record( capturedate, points )
			self.data[ recording_id ] = record

	def iterparent(self, tree):
		for parent in tree.iter('CoordinateData'):
			for child in parent:
				yield parent, child


class Record:
    
	def __init__(self, date, points):
		self.capturedate = date
		self.points = points


